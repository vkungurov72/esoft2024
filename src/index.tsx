import React from 'react'
import ReactDOM from 'react-dom/client'
import './01_app/index.css'
import App from './01_app/App'

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)
root.render(<App />)
