import { action, makeAutoObservable, observable } from 'mobx'
import { z } from 'zod'

const emptyStringToUndefined = (value: string | undefined) => {
  return value?.trim() === '' ? undefined : value
}

export const PasswordDataZod = z
  .object({
    password: z
      .string({
        required_error: 'Обязятельное поле'
      })
      .min(5, 'Мин длина пароля 5 символов'),
    confirmation_password: z.string({
      required_error: 'Обязятельное поле'
    })
  })
  .refine(date => date.password === date.confirmation_password, {
    path: ['confirmation_password'],
    message: 'Пароли не совпадают'
  })

const AuthFormData = z
  .object({
    login: z
      .string({
        required_error: 'Обязятельное поле'
      })
      .min(10, 'Мин длина логина 10 символов'),
    first_name: z.string({
      required_error: 'Обязятельное поле'
    }),
    second_name: z.string({
      required_error: 'Обязятельное поле'
    }),
    telephone: z.string(),
    email: z
      .string({
        required_error: 'Обязятельное поле'
      })
      .email({ message: 'Неверно указана почта' })
  })
  .and(PasswordDataZod)

export const FormDataZod = z
  .object({
    login: z.string().transform(emptyStringToUndefined),
    first_name: z.string().transform(emptyStringToUndefined),
    second_name: z.string().transform(emptyStringToUndefined),
    telephone: z.string(),
    email: z.string().transform(emptyStringToUndefined),
    password: z.string().transform(emptyStringToUndefined),
    confirmation_password: z.string().transform(emptyStringToUndefined)
  })
  .pipe(AuthFormData)

export type UserFormData = z.input<typeof FormDataZod>

type UserFormDataErrorType =
  | {
      login?: z.ZodIssue[] | undefined
      first_name?: z.ZodIssue[] | undefined
      second_name?: z.ZodIssue[] | undefined
      telephone?: z.ZodIssue[] | undefined
      email?: z.ZodIssue[] | undefined
      password?: z.ZodIssue[] | undefined
      confirmation_password?: z.ZodIssue[] | undefined
    }
  | undefined

export class UserStore {
  @observable login: string = ''
  @observable password: string = ''
  @observable confirmation_password: string = ''
  @observable first_name: string = ''
  @observable second_name: string = ''
  @observable telephone: string = ''
  @observable email: string = ''
  @observable errorForm: UserFormDataErrorType = undefined
  constructor () {
    makeAutoObservable(this)
  }

  @action
  getFormData () {
    return {
      login: this.login,
      password: this.password,
      confirmation_password: this.confirmation_password,
      first_name: this.first_name,
      second_name: this.second_name,
      telephone: this.telephone,
      email: this.email
    }
  }

  @action
  setError (errorForm: UserFormDataErrorType) {
    this.errorForm = errorForm
  }

  @action
  setLogin (login: string) {
    this.login = login
  }
  @action
  setPassword (password: string) {
    this.password = password
  }

  @action
  setConfirmationPassword (password: string) {
    this.confirmation_password = password
  }
  @action
  setFirstName (first_name: string) {
    this.first_name = first_name
  }
  @action
  setSecondName (second_name: string) {
    this.second_name = second_name
  }

  @action
  setTelephone (telephone: string) {
    this.telephone = telephone
  }

  @action
  setEmail (email: string) {
    this.email = email
  }

  @action
  makeRegistration (data: UserFormData) {
    console.log(data)
  }
}
