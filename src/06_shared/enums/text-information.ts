export enum TextInformation {
    LOGIN = "Логин",
    PASSWORD = "Пароль",
    CONFIRMATION_PASSWORD = "Потверждение пароля",
    TELEPHONE = "Телефон",
    PASPORT = "Паспортные данные",
    FIRST_NAME = 'Имя',
    SECOND_NAME = "Фамилия", 
    MAIL = "Почта",
}