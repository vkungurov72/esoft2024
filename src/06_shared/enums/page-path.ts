
export enum PagePath {
    ROOT ='/',
    LOGIN = '/login',
    REGISTRATION = '/registration'
}