import { Button, TextField } from '@mui/material'
import { TextInformation } from '../../06_shared/enums/text-information'
import { IMaskInput } from 'react-imask'
import { observer } from 'mobx-react'
import { useRootStore } from '../../01_app/roote-store'
import { ZodIssue } from 'zod'
import { FormDataZod } from '../../05_enitities/user/model/user-store'
import { match } from 'ts-pattern'
import { reaction } from 'mobx'
const TextMaskTelephone = (props: any) => {
  const { inputRef, ...other } = props
  return <IMaskInput {...other} mask='+{7}(000)000-00-00' />
}

const concatErrors = (
  errors: ZodIssue[] | undefined,
  showFirstError: boolean = true
) => {
  if (!errors) {
    return ''
  }

  return match({ showFirstError })
    .with({ showFirstError: true }, () => errors[0].message)
    .otherwise(() => errors.map(error => error.message).join(', '))
}

const RegistrationPage = observer(() => {
  const { userStore } = useRootStore()

  reaction(
    () => [
      userStore.login,
      userStore.password,
      userStore.confirmation_password,
      userStore.email,
      userStore.telephone,
      userStore.first_name,
      userStore.second_name
    ],
    () => {
      if (!userStore.errorForm) {
        return
      }
      userStore.setError(undefined)
    }
  )

  return (
    <div>
      <TextField
        error={!!userStore.errorForm?.login}
        helperText={concatErrors(userStore.errorForm?.login)}
        id='login'
        label={TextInformation.LOGIN}
        variant='filled'
        value={userStore.login}
        onChange={e => userStore.setLogin(e.target.value)}
      />
      <TextField
        id='password'
        error={!!userStore.errorForm?.password}
        helperText={concatErrors(userStore.errorForm?.password)}
        label={TextInformation.PASSWORD}
        type='password'
        variant='filled'
        value={userStore.password}
        onChange={e => userStore.setPassword(e.target.value)}
      />
      <TextField
        id='confirmation_password'
        error={!!userStore.errorForm?.confirmation_password}
        helperText={concatErrors(userStore.errorForm?.confirmation_password)}
        label={TextInformation.CONFIRMATION_PASSWORD}
        type='password'
        variant='filled'
        value={userStore.confirmation_password}
        onChange={e => userStore.setConfirmationPassword(e.target.value)}
      />
      <TextField
        id='email'
        error={!!userStore.errorForm?.email}
        helperText={concatErrors(userStore.errorForm?.email)}
        label={TextInformation.MAIL}
        variant='filled'
        type='email'
        value={userStore.email}
        onChange={e => userStore.setEmail(e.target.value)}
      />
      <TextField
        id='telephone'
        label={TextInformation.TELEPHONE}
        variant='filled'
        InputProps={{
          inputComponent: TextMaskTelephone
        }}
        value={userStore.telephone}
        onChange={e => userStore.setTelephone(e.target.value)}
      />
      <TextField
        id='first_name'
        label={TextInformation.FIRST_NAME}
        error={!!userStore.errorForm?.first_name}
        helperText={concatErrors(userStore.errorForm?.first_name)}
        variant='filled'
        value={userStore.first_name}
        onChange={e => userStore.setFirstName(e.target.value)}
      />
      <TextField
        id='second_name'
        error={!!userStore.errorForm?.second_name}
        helperText={concatErrors(userStore.errorForm?.second_name)}
        label={TextInformation.SECOND_NAME}
        variant='filled'
        value={userStore.second_name}
        onChange={e => userStore.setSecondName(e.target.value)}
      />
      <Button
        onClick={() => {
          const validationResult = FormDataZod.safeParse(
            userStore.getFormData()
          )
          const errors = validationResult.success
            ? undefined
            : validationResult.error.flatten((issue: ZodIssue) => issue)
                .fieldErrors

          if (errors) {
            userStore.setError(errors)
            return
          }
        }}
        variant='outlined'
      >
        Зарегистрироваться
      </Button>
    </div>
  )
})

export default RegistrationPage
