import { createBrowserRouter } from 'react-router-dom'
import AuthPage from '../../../02_pages/login/login-page'
import { PagePath } from '../../../06_shared/enums/page-path'
import RegistrationPage from '../../../02_pages/registration/registration-page'

export const router = createBrowserRouter([
  {
    id: 'root',
    path: PagePath.ROOT,
    children: [
      {
        path: PagePath.LOGIN,
        Component: AuthPage
      },
      {
        path: PagePath.REGISTRATION,
        Component: RegistrationPage
      }
    ]
  }
])
