import { ThemeProvider } from '@mui/material'
import { theme } from '../06_shared/theme/theme'
import { RouterProvider } from 'react-router-dom'
import { router } from './providers/router/router'
import { RootStoreProvider } from './roote-store'

function App () {
  return (
    <RootStoreProvider>
      <ThemeProvider theme={theme}>
        <RouterProvider router={router}></RouterProvider>
      </ThemeProvider>
    </RootStoreProvider>
  )
}

export default App
