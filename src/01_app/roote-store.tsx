import { ReactElement, createContext, useContext } from 'react'
import { UserStore } from '../05_enitities/user'

class RootStore {
  constructor (public readonly userStore: UserStore) {}
}

const userStore = new UserStore()
const rootStore = new RootStore(userStore)
export const useRootStore = () => useContext(RootStoreContext)

export const RootStoreContext = createContext<RootStore>({} as RootStore)

export const RootStoreProvider: React.FC<
  React.PropsWithChildren<Record<string, ReactElement>>
> = ({ children }) => {
  return (
    <RootStoreContext.Provider value={rootStore}>
      {children}
    </RootStoreContext.Provider>
  )
}
